# Color-Scripts-Launcher

Preview color scripts before launching them 

## Screenshot

![placeholder](screenshot.png)

## How to

Dependencies: [`fzf`](https://github.com/junegunn/fzf)

Edit `SCRIPT_LOCATION` in `color` to the same location "color-scripts" are located.

You can also use your own color scripts other than the [ones](https://gitlab.com/dwt1/shell-color-scripts) collected by dwt1

## Credit

[fzf](https://github.com/junegunn/fzf) by junegunn

[Shell Color Scripts](https://gitlab.com/dwt1/shell-color-scripts) by dwt1
